
public class Listing {
	private int id;
	private float amount;
	private Article article;

	public float priceBrutto(){
		return this.article.priceBrutto();
	}
	
	public float priceNetto(){
		return this.article.priceNetto();
	}
	
	// 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getamount() {
		return amount;
	}

	public void setamount(float amount) {
		this.amount = amount;
	}

	public Article getArtykul() {
		return article;
	}

	public void setArtykul(Article article) {
		this.article = article;
	}
	
}
