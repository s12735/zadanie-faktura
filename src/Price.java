public class Price {
	private float netto, brutto, tax;

	public Price(float netto, float brutto, float tax) {
		this.netto = netto;
		this.brutto = brutto;
		this.tax = tax;
	}


	public float getBrutto() {
		return brutto;
	}

	public float getNetto() {
		return netto;
	}

	public float getTax() {
		return tax;
	}

	// auto-gen

	public void setNetto(float netto) {
		this.netto = netto;
	}

	public void setBrutto(float brutto) {
		this.brutto = brutto;
	}

	public void settax(float tax) {
		this.tax = tax;
	}

}
