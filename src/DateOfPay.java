import java.util.Date;

public class DateOfPay {
	private int left;
	private Date data;

	public DateOfPay(int dni) {
		this.left = dni;
	}

	public DateOfPay(Date data) {
		this.data = data;
	}

	public DateOfPay(int dni, Date data) {
		this.left = dni;
		this.data = data;
	}
 
	//
	public int getleft() {
		return left;
	}

	public void setleft(int left) {
		this.left = left;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	
}
