
public class Article {
	private String nazwa;
	private Unit unit;
	private Price price;

	public Article(String nazwa, Unit unit, Price price) {
		this.nazwa = nazwa;
		this.unit = unit;
		this.price = price;
	}
	
	public float priceBrutto(){
		return price.getBrutto();
	}
	
	public float priceNetto(){
		return price.getNetto();
	}
	
	public float priceTax(){
		return price.getTax();
	}

	// auto-generated
	
	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Unit getJednostka() {
		return unit;
	}

	public void setJednostka(Unit unit) {
		this.unit = unit;
	}

	public Price getPrice() {
		return price;
	}

	public void setPrice(Price price) {
		this.price = price;
	}
		
	
}
