import java.util.Date;
import java.util.List;

public class Invoice {
	private String number;
	private List<Listing> position;
	private Date dateOfSale;
	private Price price;
	private DateOfPay dateOfPay;
	private float sumaBrutto = 0;
	private float sumaNetto = 0;

	public Invoice(String number, List<Listing> position, Date dateOfSale, DateOfPay dateOfPay) {
		this.number = number;
		this.position = position;
		this.dateOfSale = dateOfSale;
		this.dateOfPay = dateOfPay;
		
		for(Listing positionAtList : this.position){
			sumaBrutto = sumaBrutto + positionAtList.priceBrutto();
			sumaNetto = sumaNetto + positionAtList.priceNetto();
		}
		
		this.price = new Price(sumaNetto, sumaBrutto, 0);
	}

	//
	public String getnumber() {
		return number;
	}

	public void setnumber(String number) {
		this.number = number;
	}

	public List<Listing> getposition() {
		return position;
	}

	public void setPosition(List<Listing> position) {
		this.position = position;
	}

	public Date getdateOfSaled() {
		return dateOfSale;
	}

	public void setdateOfSaled(Date dateOfSale) {
		this.dateOfSale = dateOfSale;
	}

	public Price getCena() {
		return price;
	}

	public void setCena(Price price) {
		this.price = price;
	}

	public DateOfPay getDataPlatnosci() {
		return dateOfPay;
	}

	public void setDataPlatnosci(DateOfPay dateOfPay) {
		this.dateOfPay = dateOfPay;
	}

	public float getSumaBrutto() {
		return sumaBrutto;
	}

	public void setSumaBrutto(float sumaBrutto) {
		this.sumaBrutto = sumaBrutto;
	}

	public float getSumaNetto() {
		return sumaNetto;
	}

	public void setSumaNetto(float sumaNetto) {
		this.sumaNetto = sumaNetto;
	}	
}
